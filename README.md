## Run these commands for run project      
#### 1 - composer i
#### 2 - npm i & npm run build
#### 3 - cp .env.example .env
#### 4 - set a value to DB_DATABASE in env
#### 5 - php artisan key:generate
#### 6 - php artisan storage:link
#### 7 - php artisan migrate --seed
#### 8 - php artisan serve

##  For login, use this credential 
#### email : admin@admin.com
#### password : password

##  For run test, run this command 
#### php artisan test

NOTE: be sure that you install pdo_sqlite extension in your php environment
