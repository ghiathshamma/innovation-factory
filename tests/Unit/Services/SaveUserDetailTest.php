<?php
namespace Tests\Unit\Services;

use App\Events\UserSaved;
use App\Listeners\SaveUserBackgroundInformation;
use App\Models\Detail;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Event;

class SaveUserDetailTest extends TestCase
{
    use RefreshDatabase, WithFaker;


    /**
     * @test
     * @return void
     */

    public function it_saves_user_detail_when_a_user_is_saved()
    {
        $user = User::factory()->create();
        Event::fake();
        event(new UserSaved($user));
        Event::assertDispatched(UserSaved::class);
        Event::assertListening(UserSaved::class, SaveUserBackgroundInformation::class);
        Detail::where('user_id',$user->id)->count();
        $this->assertDatabaseCount('details', $user->details()->count());
    }

}
