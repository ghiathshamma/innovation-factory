<?php
namespace Tests\Unit\Services;

use App\Models\User;
use App\Services\UserService;
use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class UserServiceTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public $userService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userService=new UserService(new User() , request());
        Artisan::call('storage:link');
    }

    /**
     * @test
     * @return void
     */
    public function it_can_return_a_paginated_list_of_users()
    {
        User::factory()->count(10)->create();
        $usersTrashed = $this->userService->list()->paginate(10);
        $this->assertInstanceOf(LengthAwarePaginator::class,$usersTrashed);
    }

    /**
     * @test
     * @return void
     */

    public function it_can_store_a_user_to_database()
    {
        $data = User::factory()->raw();
        $user=$this->userService->store($data);
        $this->assertDatabaseHas('users',[
            'id' => $user->id,
            'email' => $data['email']
        ]);
    }

    /**
     * @test
     * @return void
     */
    public function it_can_find_and_return_an_existing_user()
    {
        $user=User::factory()->create();
        $returned_user=$this->userService->find($user->id);
        $this->assertEquals($returned_user->id,$user->id);
    }

    /**
     * @test
     * @return void
     */
    public function it_can_update_an_existing_user()
    {
        $user = User::factory()->create();
        $this->userService->update($user->id,['username'=>'updated username']);
        $this->assertDatabaseHas('users',['id'=> $user->id , 'username' => 'updated username']);
    }
    /**
     * @test
     * @return void
     */
    public function it_can_soft_delete_an_existing_user()
    {
        $user = User::factory()->create();
        $this->userService->destroy($user->id);
        $this->assertSoftDeleted('users',['id'=>$user->id]);
    }

    /**
     * @test
     * @return void
     */
    public function it_can_restore_a_soft_deleted_user()
    {
        $user = User::factory()->create();
        $this->userService->destroy($user->id);
        $this->userService->restore($user->id);
        $this->assertNotSoftDeleted('users',['id'=>$user->id]);
    }
    /**
     * @test
     * @return void
     */
    public function it_can_permanently_delete_a_soft_deleted_user()
    {
        $user = User::factory()->create();
        $this->userService->delete($user->id);
        $query = $this->userService->listTrashed();
        $found = $query->where('id',$user->id)->first();
        $this->assertEmpty($found);
    }

    /**
     * @test
     * @return void
     */
    public function it_can_upload_photo()
    {
        Storage::fake('public');
        $file = UploadedFile::fake()->image('test.jpg');
        $file_name = $this->userService->upload($file);
        Storage::disk('public')->assertExists($file_name);
    }

    /**
     * @test
     * @return void
     */
    public function it_can_save_details()
    {
        $user = User::factory()->create();
        $result=$this->userService->saveUserDetails($user);
        $this->assertInstanceOf(Collection::class,$result);
    }
    /**
     * @test
     * @return void
     */
    public function it_can_return_a_paginated_list_of_trashed_users()
    {
        User::factory()->count(10)->state(['deleted_at' => now()])->create();
        $usersTrashed = $this->userService->listTrashed()->paginate(10);
        $this->assertInstanceOf(LengthAwarePaginator::class,$usersTrashed);
    }
}
