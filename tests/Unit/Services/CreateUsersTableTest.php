<?php
namespace Tests\Unit\Services;

use Illuminate\Support\Facades\Schema;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class CreateUsersTableTest extends TestCase
{
    use RefreshDatabase, WithFaker;


    /**
     * @test
     * @return void
     */

    public function it_has_expected_columns()
    {
        $this->artisan('migrate');
        $columns = Schema::getColumnListing('users');
        $expectedColumns = [
            'id',
            'prefixname',
            'firstname',
            'middlename',
            'lastname',
            'suffixname',
            'username',
            'email',
            'password',
            'photo',
            'type',
            'remember_token',
            'email_verified_at',
            'created_at',
            'updated_at',
            'deleted_at',
            'two_factor_secret',
            'two_factor_recovery_codes',
            'two_factor_confirmed_at',
        ];

        // Assert that the columns match the expected columns
        $this->assertEquals($expectedColumns, $columns);
    }

}
