<?php
namespace App\Enums;
final class UserEnum
{
    const MR = 'Mr';
    const MRS = 'Mrs';
    const MS = 'Ms';

    public static function getPrefixName()
    {
        return [
            self::MR,
            self::MRS,
            self::MS,
        ];
    }
}
