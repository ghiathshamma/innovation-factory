<?php

namespace App\Listeners;

use App\Services\UserService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SaveUserBackgroundInformation
{


    private $UserService;

    public function __construct(UserService $userService)
    {
        $this->UserService=$userService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $this->UserService->saveUserDetails($event->user);
    }
}
