<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


trait FileHandler
{
    public function storeFile($file, $dir){
        $this->checkDir($dir);
        $name = $dir . '/' . $file->hashName();
        Image::make($file)->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path('app/public/') . $name);
        return $name;
    }

    public  function updateFile($new_file, $old_file, $dir){
        if($old_file)
            $this->deleteFile($old_file);
        $name=$this->storeFile($new_file,$dir);
        return $name;
    }

    public  function deleteFile($file)
    {
        if ($file)
            if (file_exists(storage_path('app/public/') . $file)) {
                Storage::disk('public')->delete($file);
                return true;
            }
        return false;
    }
    private function checkDir($dir)
    {
        if(!File::isDirectory($dir))
            Storage::disk('public')->makeDirectory($dir);
    }
}

