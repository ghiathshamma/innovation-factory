<?php

namespace App\Http\Controllers\web;

use App\DataTables\UserDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\user\UserRequest;
use App\Models\User;
use App\Services\UserService;

class UserController extends Controller
{
    private $UserService;
    public function __construct(UserService $UserService)
    {
        $this->UserService=$UserService;
    }

    public function index(UserDataTable $dataTable )
    {
        return $dataTable->render('users.index');
    }
    public function create()
    {
        return view('users.create');
    }
    public function store(UserRequest $request)
    {
        $this->UserService->store($request->validated());
        return redirect(route('users.index'));
    }
    public function edit(User $user)
    {
        return view('users.edit',compact('user'));
    }
    public function show(User $user)
    {
        return view('users.show',compact('user'));
    }
    public function update($id, UserRequest $request)
    {
        $this->UserService->update($id,$request->validated());
        return redirect(route('users.index'));
    }
    public function destroy($id)
    {
        $this->UserService->destroy($id);
        return redirect(route('users.index'));
    }
    public function trashed(UserDataTable $dataTable)
    {
        return $dataTable->render('users.index');
    }
    public function restore($id)
    {
        $this->UserService->restore($id);
        return redirect(route('users.index'));
    }

    public function delete($id)
    {
        $this->UserService->delete($id);
        return redirect(route('users.index'));
    }
}
