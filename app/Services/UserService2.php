<?php

namespace App\Services;


use App\Models\User;
use App\Traits\FileHandler;


class UserService2
{
    use FileHandler;
    public function store($data)
    {
        if(isset($data['photo']))
            $data['photo']=$this->storeFile($data['photo'],'users/photos');
        $data['password']=bcrypt($data['password']);
        if(is_null($data['type']))
            unset($data['type']);
        return User::create($data);
    }

    public function update($data, $model)
    {
        if(isset($data['photo']))
            $data['photo']=$this->updateFile($data['photo'],$model->photo,'users/photos');
        if(isset($data['password']))
            $data['password']=bcrypt($data['password']);
        else
            $data['password']=$model->password;
        $model->update($data);
        return $model;
    }

    public function destroy($model)
    {
//        if($model->logo)
//          $this->deleteFile($model->logo);
        $model->delete();
    }

}
