<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class UserService implements UserServiceInterface
{

    /**
     * The model instance.
     *
     * @var \App\Models\User
     */
    protected $model;

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Constructor to bind model to a repository.
     *
     * @param \App\User $model
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(User $model, Request $request)
    {
        $this->model = $model;
        $this->request = $request;
    }

    /**
     * Define the validation rules for the model.
     *
     * @param int $id
     * @return array
     */
    public function rules($id = null)
    {
        $updateRules = [];
        if ($id) {
            $updateRules = [
                'username' => 'required|string|' . Rule::unique('users')->ignore($id),
                'email' => 'required|email|' . Rule::unique('users')->ignore($id),
                'password' => 'nullable|confirmed',
            ];
        }
        return array_merge([
            'prefixname' => 'nullable|string|in:Mr,Mrs,Ms',
            'firstname' => 'required|string',
            'middlename' => 'nullable|string',
            'lastname' => 'required|string',
            'suffixname' => 'nullable|string',
            'username' => 'required|string|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'photo' => 'nullable|file|mimes:png,jpg,jpeg',
            'type' => 'nullable|string',
        ], $updateRules);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function list()
    {
        return $this->model::query();
    }

    /**
     * Create model resource.
     *
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store(array $attributes)
    {
        if (isset($attributes['photo']))
            $attributes['photo'] = $this->upload($attributes['photo']);
        $attributes['password'] = $this->hash($attributes['password']);
        if (isset($attributes['type']) && is_null($attributes['type']))
            unset($attributes['type']);
        return User::create($attributes);
    }

    /**
     * Retrieve model resource details.
     * Abort to 404 if not found.
     *
     * @param integer $id
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function find(int $id): User
    {
        return $this->model->where('id',$id)->first();
    }

    /**
     * Update model resource.
     *
     * @param integer $id
     * @param array $attributes
     * @return boolean
     */
    public function update(int $id, array $attributes): bool
    {
        try {
            $model = $this->find($id);
            if (isset($attributes['photo'])) {
                $this->deleteFile($model->getAttributes()['photo']);
                $attributes['photo'] = $this->upload($attributes['photo']);
            }
            if (isset($attributes['password']))
                $attributes['password'] = $this->hash($attributes['password']);
            else
                $attributes['password'] = $model->password;
            $model->update($attributes);
            return true;
        }catch (\Exception $e)
        {
            return  false;
        }
    }

    /**
     * Soft delete model resource.
     *
     * @param integer|array $id
     * @return void
     */
    public function destroy($id)
    {
        $model = $this->find($id);
        $model->delete();
    }

    /**
     * Include only soft deleted records in the results.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function listTrashed()
    {
        return $this->model::onlyTrashed();
    }

    /**
     * Restore model resource.
     *
     * @param integer|array $id
     * @return void
     */
    public function restore($id)
    {
        return User::withTrashed()->find($id)->restore();
    }

    /**
     * Permanently delete model resource.
     *
     * @param integer|array $id
     * @return void
     */
    public function delete($id)
    {
        $model = User::withTrashed()->find($id);
        if ($model->photo)
            $this->deleteFile($model->getAttributes()['photo']);
        $model->forceDelete();
    }

    /**
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Collection
     */

    public function saveUserDetails(User $user)
    {
        $details=[
            ['key'=>'Full name','value'=>$user->fullname],
            ['key'=>'Middle Initial','value'=>$user->middleinitial],
            ['key'=>'Avatar','value'=>$user->photo],
            ['key'=>'Gender','value'=>$user->gender],
        ];
        $details=$user->details()->createMany($details);
        return $details;
    }
    /**
     * Generate random hash key.
     *
     * @param string $key
     * @return string
     */
    public function hash(string $key): string
    {
        return bcrypt($key);
    }

    /**
     * Upload the given file.
     *
     * @param \Illuminate\Http\UploadedFile $file
     * @return string|null
     */
    public function upload(UploadedFile $file)
    {
        try {
            return $file->store('users/photos', 'public');
        }catch (\Exception $e)
        {
            return null;
        }
    }

    public function deleteFile($path)
    {
        try {
            if ($path)
                if (file_exists(storage_path('app/public/') . $path)) {
                    Storage::disk('public')->delete($path);
                    return true;
                }

        } catch (\Exception $e) {
            return false;
        }
    }

}
