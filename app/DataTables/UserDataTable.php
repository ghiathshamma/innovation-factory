<?php

namespace App\DataTables;

use App\Models\User;
use app\Services\UserService;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', function ($item) {
                $actions = '<a class="edit btn btn-xs btn-primary" href="' . route('users.edit', $item->id) . '" style="color:#fff">Edit</a>
                        <a class="edit btn btn-xs btn-info" href="' . route('users.show', $item->id) . '" style="color:#fff">Show</a>
                            <form action="' . route('users.destroy', $item->id) . '" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="' . csrf_token() . '">
                            <input type="submit" class="delete btn btn-xs btn-dark" style="color:#fff" value="Delete">
                            </form>';
                if (Str::contains(request()->fullUrl(), route('users.trashed')))
                    $actions = '
                            <form action="' . route('users.restore', $item->id) . '" method="post">
                            <input type="hidden" name="_method" value="PATCH">
                            <input type="hidden" name="_token" value="' . csrf_token() . '">
                            <input type="submit" class="delete btn btn-xs btn-dark" style="color:#fff" value="Restore">
                            </form>
                             <form action="' . route('users.delete', $item->id) . '" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="' . csrf_token() . '">
                            <input type="submit" class="delete btn btn-xs btn-dark" style="color:#fff" value="Delete">
                            </form>';
                return $actions;
            })
            ->addColumn('photo', function ($item) {
                return '<img width="100"  src="' . $item->photo . '">';
            })->addColumn('fullname', function ($item) {
                return $item->fullname;
            })
            ->setRowId('id')->rawColumns(['photo', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Company $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(UserService $model): QueryBuilder
    {
        if(Str::contains(request()->fullUrl() , route('users.trashed')) ) {
            return $model->listTrashed();
        }
        return $model->list();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('user-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(0)
                    ->selectStyleSingle()
                    ->buttons([
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make('id'),
            Column::make('fullname'),
            Column::make('firstname'),
            Column::make('lastname'),
            Column::make('username'),
            Column::make('email'),
            Column::make('photo'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'User_' . date('YmdHis');
    }

    public function excel()
    {
        // TODO: Implement excel() method.
    }

    public function csv()
    {
        // TODO: Implement csv() method.
    }

    public function pdf()
    {
        // TODO: Implement pdf() method.
    }
}
