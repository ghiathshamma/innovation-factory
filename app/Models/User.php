<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Enums\UserEnum;
use App\Events\UserSaved;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Casts\Attribute;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dispatchesEvents=[
        'saved' => UserSaved::class,
    ];
    /**
     * Get the user's first name.
     *
     * @param  string  $value
     * @return string
     */
    public function getPhotoAttribute($value)
    {
        return $value != null ? asset('storage/' . $value) : asset('default_images/user.jpg');
    }

    public function getFullnameAttribute()
    {
        return Str::replace('  ',' ',$this->firstname .' '.$this->middleinitial.' '.$this->lastname);
    }
    public function getMiddleinitialAttribute ()
    {
        $firstChar = Str::substr($this->middlename, 0, 1);
        return $firstChar ? Str::upper($firstChar) . '.' : '';
    }
    public function details()
    {
        return $this->hasMany(Detail::class);
    }
    public function getGenderAttribute()
    {
        return Str::lower($this->prefixname)==Str::lower(UserEnum::MR)?'Male':'Female';
    }
}
