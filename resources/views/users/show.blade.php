@extends('layouts.dashboard.app')
@section('content')
    <div class="container p-4">
        <form  method="POST" action="{{route('users.update',$user->id)}}" enctype="multipart/form-data">
            @csrf()
            @method('PUT')
            <div class="row">
                <div class="form-group ">
                    <label class="required-label">prefix name : {{$user->prefixname}}</label>
                </div>
                <div class="form-group ">
                    <label class="required-label">first name : {{$user->firstname}}</label>
                </div>
                <div class="form-group ">
                    <label class="required-label">middle name : {{$user->middlename}}</label>
                </div>
                <div class="form-group ">
                    <label class="required-label">last name : {{$user->lastname}}</label>
                </div>
                <div class="form-group ">
                    <label class="required-label">suffix name : {{$user->suffixname}}</label>
                </div>
                <div class="form-group ">
                    <label class="required-label">username : {{$user->username}}</label>
                </div>
                <div class="form-group ">
                    <label class="required-label">email : {{$user->email}}</label>
                </div>
                <div class="form-group ">
                    <label class="required-label">type : {{$user->type}}</label>
                </div>
                <div class="form-group ">
                    <label for="date-format">photo</label>
                    <div>
                        <img src="{{$user->photo}}" alt="{{$user->username}}" width="150" height="150">
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
