@extends('layouts.dashboard.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Manage Companies</div>
            <div class="card-body">
                <button class="btn btn-success"><a style="color: white;text-decoration: none" href="{{route('users.create')}}">Add User</a></button>
                @if(Str::contains(request()->fullUrl() , route('users.trashed')))
                    <button class="btn btn-success"><a style="color: white;text-decoration: none" href="{{route('users.index')}}">users</a></button>
                @else
                    <button class="btn btn-success"><a style="color: white;text-decoration: none" href="{{route('users.trashed')}}">trashed</a></button>
                @endif

                {{ $dataTable->table() }}
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
@endpush
