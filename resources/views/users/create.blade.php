@extends('layouts.dashboard.app')
@section('content')
    <div class="container p-4">
        <form  method="POST" action="{{route('users.store')}}" enctype="multipart/form-data">
            @csrf()
            <div class="row">
                <div class="form-group ">
                    <label class="required-label">prefix name</label>
                    <select name="prefixname" class="form-control">
                        @foreach(\App\Enums\UserEnum::getPrefixName() as $prefix)
                            <option value="{{$prefix}}">{{$prefix}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group ">
                    <label class="required-label">first name</label>
                    <input type="text" name="firstname" class="form-control" value="{{old('firstname')}}">
                </div>
                <div class="form-group ">
                    <label class="required-label">middlename</label>
                    <input type="text" name="middlename" class="form-control" value="{{old('middlename')}}">
                </div>
                <div class="form-group ">
                    <label class="required-label">last name</label>
                    <input type="text" name="lastname" class="form-control" value="{{old('lastname')}}">
                </div>
                <div class="form-group ">
                    <label class="required-label">suffix name</label>
                    <input type="text" name="suffixname" class="form-control" value="{{old('suffixname')}}">
                </div>
                <div class="form-group ">
                    <label class="required-label">username</label>
                    <input type="text" name="username" class="form-control" value="{{old('username')}}">
                </div>
                <div class="form-group ">
                    <label class="required-label">email</label>
                    <input type="email" name="email" class="form-control" value="{{old('email')}}">
                </div>
                <div class="form-group ">
                    <label class="required-label">password</label>
                    <input type="password" name="password" class="form-control">
                </div>
                <div class="form-group ">
                    <label class="required-label">password confirmation </label>
                    <input type="password" name="password_confirmation" class="form-control">
                </div>
                <div class="form-group ">
                    <label class="required-label">type</label>
                    <input type="text" name="type" class="form-control" value="{{old('type')}}">
                </div>
                <div class="form-group ">
                    <label for="date-format">photo</label>
                    <div>
                        <input type="file" name="photo" class="form-control floating-label date-format" placeholder="Begin Date Time">
                    </div>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    <button type="submit" class=" btn btn-xs btn-primary px-5 mr-2 form-control">
                        Add
                    </button>
                </div>

            </div>
        </form>
    </div>
@stop
